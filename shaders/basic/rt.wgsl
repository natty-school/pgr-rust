struct CameraUniform {
    view_proj: mat4x4<f32>,
};

@group(0) @binding(0)
var<uniform> camera: CameraUniform;

struct SpecialUniform {
    t: f32,
    center_x: f32,
    center_y: f32,
};

struct VertexInput {
    @location(0) position: vec3<f32>,
    @location(1) tex_coords: vec2<f32>,
}

struct VertexOutput {
    @builtin(position) clip_position: vec4<f32>,
    @location(0) tex_coords: vec2<f32>,
}

@vertex
fn vs_main(
    model: VertexInput,
) -> VertexOutput {
    var out: VertexOutput;
    out.tex_coords = model.tex_coords;
    out.clip_position = camera.view_proj * vec4<f32>(model.position, 1.0);
    return out;
}

@group(1) @binding(0)
var<uniform> special: SpecialUniform;

struct Ray {
    origin: vec3<f32>,
    direction: vec3<f32>,
};

struct Sphere {
    center: vec3<f32>,
    radius: f32,
};

fn intersect_sphere(
    sphere: ptr<function, Sphere>,
    ray: ptr<function, Ray>,
    normal: ptr<function, vec3<f32>>,
) -> f32 {
    var oc: vec3<f32> = (*ray).origin - (*sphere).center;
    var a: f32 = 1.0;
    var b: f32 = 2.0 * dot(oc, (*ray).direction);
    var c: f32 = dot(oc, oc) - (*sphere).radius * (*sphere).radius;
    var discriminant: f32 = b * b - 4.0 * a * c;
    if (discriminant < 0.0) {
        return -1.0;
    }
    var t: f32 = (-b - sqrt(discriminant)) / (2.0 * a);
    if (t < 0.0) {
        return -1.0;
    }
    *normal = normalize(oc + t * (*ray).direction);
    return t;
}

fn intersect_infinite_cylinder(
    tangent: vec3<f32>,
    radius: f32,
    ray: ptr<function, Ray>,
    normal: ptr<function, vec3<f32>>,
) -> f32 {
    var d = (*ray).direction;
    var o = (*ray).origin;

    var a: f32 = 1.0 - dot(d, tangent) * dot(d, tangent);
    var b: f32 = 2.0 * (dot(d, o) - dot(d, tangent) * dot(o, tangent));
    var c: f32 = dot(o, o) - dot(o, tangent) * dot(o, tangent) - radius * radius;
    var discriminant: f32 = b * b - 4.0 * a * c;
    if (discriminant < 0.0) {
        return -1.0;
    }
    var t: f32 = (-b - sqrt(discriminant)) / (2.0 * a);
    if (t < 0.0) {
        return -1.0;
    }
    *normal = normalize(o + t * d - tangent * dot(o + t * d, tangent));
    return t;
}

fn intersect_inner_infinite_cylinder(
    tangent: vec3<f32>,
    radius: f32,
    ray: ptr<function, Ray>,
    normal: ptr<function, vec3<f32>>,
) -> f32 {
    var d = (*ray).direction;
    var o = (*ray).origin;

    var a: f32 = 1.0 - dot(d, tangent) * dot(d, tangent);
    var b: f32 = 2.0 * (dot(d, o) - dot(d, tangent) * dot(o, tangent));
    var c: f32 = dot(o, o) - dot(o, tangent) * dot(o, tangent) - radius * radius;
    var discriminant: f32 = b * b - 4.0 * a * c;
    if (discriminant < 0.0) {
        return -1.0;
    }
    var t: f32 = (-b + sqrt(discriminant)) / (2.0 * a);
    if (t < 0.0) {
        return -1.0;
    }
    *normal = -normalize(o + t * d - tangent * dot(o + t * d, tangent));
    return t;
}

fn intersect_inwards_sphere(
    sphere: ptr<function, Sphere>,
    ray: ptr<function, Ray>,
    normal: ptr<function, vec3<f32>>,
) -> f32 {
    var oc: vec3<f32> = (*ray).origin - (*sphere).center;
    var a: f32 = 1.0;
    var b: f32 = 2.0 * dot(oc, (*ray).direction);
    var c: f32 = dot(oc, oc) - (*sphere).radius * (*sphere).radius;
    var discriminant: f32 = b * b - 4.0 * a * c;
    if (discriminant < 0.0) {
        return -1.0;
    }
    var t: f32 = (-b + sqrt(discriminant)) / (2.0 * a);
    if (t < 0.0) {
        return -1.0;
    }
    *normal = -normalize(oc + t * (*ray).direction);
    return t;
}

fn intersect_plane(
    plane: vec4<f32>,
    ray: ptr<function, Ray>,
    normal: ptr<function, vec3<f32>>,
) -> f32 {
    var denom: f32 = dot(-plane.xyz, (*ray).direction);
    if (abs(denom) > 0.0001) {
        var t: f32 = -(dot(-plane.xyz, (*ray).origin) + plane.w) / denom;
        if (t > -0.001) {
            *normal = plane.xyz;
            return t;
        }
    }
    return -1.0;
}

fn intersect_downwards_paraboloid(
    origin: vec3<f32>,
    xscale: f32,
    zscale: f32,
    ray: ptr<function, Ray>,
    normal: ptr<function, vec3<f32>>,
) -> f32 {
    var o = (*ray).origin;
    var q = o - origin;
    var d = (*ray).direction;
    var a = (d.x * d.x * xscale + d.z * d.z * zscale);
    var b = d.y + 2.0 * (q.x * d.x * xscale + q.z * d.z * zscale);
    var c = q.y + (q.x * q.x * xscale + q.z * q.z * zscale);

    var discriminant = b * b - 4.0 * a * c;

    if (discriminant < 0.0) {
        return -1.0;
    }

    var t = (-b - sqrt(discriminant)) / (2.0 * a);

    if (t < 0.0) {
        return -1.0;
    }

    *normal = normalize(vec3<f32>(
        (o.x + t * d.x - origin.x) * 2.0 * xscale,
        1.0,
        (o.z + t * d.z - origin.z) * 2.0 * zscale,
    ));

    return t;
}

fn intersect_upwards_paraboloid(
    origin: vec3<f32>,
    xscale: f32,
    zscale: f32,
    ray: ptr<function, Ray>,
    normal: ptr<function, vec3<f32>>,
) -> f32 {
    var o = (*ray).origin;
    var q = o - origin;
    var d = (*ray).direction;
    var a = -(d.x * d.x * xscale + d.z * d.z * zscale);
    var b = d.y - 2.0 * (q.x * d.x * xscale + q.z * d.z * zscale);
    var c = q.y - (q.x * q.x * xscale + q.z * q.z * zscale);

    var discriminant = b * b - 4.0 * a * c;

    if (discriminant < 0.0) {
        return -1.0;
    }

    var t = (-b - sqrt(discriminant)) / (2.0 * a);

    if (t < 0.0) {
        return -1.0;
    }

    *normal = normalize(vec3<f32>(
        -(o.x + t * d.x - origin.x) * 2.0 * xscale,
        1.0,
        -(o.z + t * d.z - origin.z) * 2.0 * zscale,
    ));

    return t;
}

struct Material {
    color: vec3<f32>,
    specular: f32,
    reflectivity: f32,
};


struct PointLight {
    position: vec3<f32>,
    color: vec3<f32>,
    strength: f32,
};


fn trace(
    tex_coords: vec2<f32>,
) -> vec4<f32> {
    var x = tex_coords.x;
    var y = tex_coords.y;

    var cx = x - 0.5;
    var cy = (1.0 - y) - 0.5;
    var zd = 1.2;

    var ray_dd = normalize(vec3<f32>(cx, cy, zd));
    var rsin = cos(special.t / 4.0);
    var rcos = sin(special.t / 4.0);
    var rddc = ray_dd * rcos;
    var rdds = ray_dd * rsin;

    var pos = vec3<f32>(15.0 * rsin, 0.5, 15.0 * -rcos);
    var dir = normalize(vec3<f32>(rddc.x - rdds.z, ray_dd.y, rdds.x + rddc.z));

    var ray = Ray(
        pos,
        dir
    );

    var ambient: vec3<f32> = vec3<f32>(0.003, 0.003, 0.003);

    var materials: array<Material, 2> = array<Material, 2>(
        Material(
            vec3<f32>(0.8, 0.3, 0.3),
            50.0,
            0.9,
        ),
        Material(
            vec3<f32>(0.9, 0.9, 0.9),
            10.0,
            0.9,
        ),
    );

    var lights: array<PointLight, 2> = array<PointLight, 2>(
        PointLight(
            vec3<f32>(-5.0, 5.5, 0.0),
            vec3<f32>(0.0, 0.0, 1.0),
            10.0
        ),
        PointLight(
            vec3<f32>(5.0, 5.0, 0.0),
            vec3<f32>(0.9, 0.0, 1.0),
            15.0
        ),
    );

    var color = vec3<f32>(0.0, 0.0, 0.0);
    var normal: vec3<f32>;
    var material_id = 0;
    var bounce = 0;
    var reflection = 1.0;

    loop {
        if (bounce >= 10) {
            break;
        }

        var t = 999999.0;
        var n = vec3<f32>(0.0);

        {
            var sphere0 = Sphere(
                vec3<f32>(4.0 * cos(special.t), 1.5, 4.0 * sin(special.t)),
                1.0,
            );

            var t0 = intersect_sphere(&sphere0, &ray, &n);
            if (t0 < t && t0 > 0.0) {
                t = t0;
                normal = n;
                material_id = 0;
            }
        }

        var sphere = Sphere(
            vec3<f32>(0.0, 1.5 + sin(special.t), 0.0),
            2.5,
        );

        var t1 = intersect_sphere(
            &sphere,
            &ray,
            &n,
        );

        if (t1 < t && t1 > 0.0) {
            t = t1;
            material_id = 0;
            normal = n;
        }

        var t2 = intersect_downwards_paraboloid(
            vec3<f32>(0.0, -5.0, 0.0),
            0.02,
            0.02,
            &ray,
            &n,
        );

        if (t2 < t && t2 > 0.0) {
            t = t2;
            material_id = 1;
            normal = n;
        }

        var t3 = intersect_plane(
            vec4<f32>(normalize(vec3<f32>(1.0, -0.15, 0.0)), -20.0),
            &ray,
            &n,
        );

        if (t3 < t && t3 > 0.0) {
            t = t3;
            material_id = 1;
            normal = n;
        }

        var t4 = intersect_plane(
            vec4<f32>(normalize(vec3<f32>(-1.0, -0.15, 0.0)), -20.0),
            &ray,
            &n,
        );

        if (t4 < t && t4 > 0.0) {
            t = t4;
            material_id = 1;
            normal = n;
        }

        var t5 = intersect_plane(
            vec4<f32>(normalize(vec3<f32>(0.0, -0.15, 1.0)), -20.0),
            &ray,
            &n,
        );

        if (t5 < t && t5 > 0.0) {
            t = t5;
            material_id = 1;
            normal = n;
        }

        var t6 = intersect_plane(
            vec4<f32>(normalize(vec3<f32>(0.0, -0.15, -1.0)), -20.0),
            &ray,
            &n,
        );

        if (t6 < t && t6 > 0.0) {
            t = t6;
            material_id = 1;
            normal = n;
        }

        if (t < 0.0 || t > 9999.0) {
            break;
        }

        var hit = ray.origin + t * ray.direction;
        var material = materials[material_id];
        var diffuse = material.color;

        var i = 0;
        loop {
            if (i >= 2) {
                break;
            }

            var light = lights[i];
            var light_dir = normalize(light.position - hit);
            var light_dist = length(light.position - hit);
            var ambient_color = ambient * diffuse;
            var diffuse_light = max(0.0, dot(normal, light_dir)) * light.strength * pow(light_dist, -2.0);
            var diffuse_color = diffuse * light.color * diffuse_light;
            var specular_light = pow(max(0.0, dot(normalize(reflect(-light_dir, normal)), -ray.direction)), material.specular);
            var specular_color = light.color * specular_light * light.strength * pow(light_dist, -2.0);
            color += reflection * (ambient_color + diffuse_color + specular_color);
            reflection *= material.reflectivity;

            i += 1;
        }

        ray.origin = hit + 0.001 * normal;
        ray.direction = reflect(ray.direction, normal);

        bounce += 1;
    }

    return vec4<f32>(color, 1.0);
}

fn rand(n: f32) -> f32 { return fract(sin(n) * 43758.5453123); }
fn noise(p: f32) -> f32 {
  let fl = floor(p);
  let fc = fract(p);
  return mix(rand(fl), rand(fl + 1.), fc);
}

@fragment
fn fs_main(in: VertexOutput) -> @location(0) vec4<f32> {
    var col = vec4<f32>(0.0, 0.0, 0.0, 1.0);

    let samples = 4;
    for (var i = 0; i < samples; i += 1) {
        var x = in.tex_coords.x + f32(i * i * i) + special.t * 10.0;
        var y = in.tex_coords.y + f32(i * i * i) + special.t * 10.0;

        var sc = vec2<f32>(in.tex_coords.x + noise(x) * 0.001, in.tex_coords.y + noise(y) * 0.001);

        col += trace(sc);
    }

    return col / f32(samples);
}