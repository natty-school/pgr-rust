struct CameraUniform {
    view_proj: mat4x4<f32>,
};

@group(0) @binding(0)
var<uniform> camera: CameraUniform;

struct SpecialUniform {
    t: f32,
    center_x: f32,
    center_y: f32,
};

struct VertexInput {
    @location(0) position: vec3<f32>,
    @location(1) tex_coords: vec2<f32>,
}

struct VertexOutput {
    @builtin(position) clip_position: vec4<f32>,
    @location(0) tex_coords: vec2<f32>,
}

@vertex
fn vs_main(
    model: VertexInput,
) -> VertexOutput {
    var out: VertexOutput;
    out.tex_coords = model.tex_coords;
    out.clip_position = camera.view_proj * vec4<f32>(model.position, 1.0);
    return out;
}

@group(1) @binding(0)
var<uniform> special: SpecialUniform;

fn fxz(x: f32, z: f32) -> f32 {
    return (sin(x) + sin(z)) * 0.25;
}

fn color(
    tex_coords: vec2<f32>,
) -> vec4<f32> {
    var x = tex_coords.x;
    var y = tex_coords.y;

    var cx = x - 0.5;
    var cy = (1.0 - y) - 0.5;
    var zd = 1.2;

    var ray_dd = normalize(vec3<f32>(cx, cy, zd));
    var rsin = cos(special.t / 4.0);
    var rcos = sin(special.t / 4.0);
    var rddc = ray_dd * rcos;
    var rdds = ray_dd * rsin;
    var dir = vec3<f32>(rddc.x - rdds.z, ray_dd.y, rdds.x + rddc.z);

    var sc = vec3<f32>(0.0, 0.0 + sin(special.t) * 3.0, 0.0);
    var r = 2.0;

    var light_pos = vec3<f32>(-7.0, 5.0, -3.0);
    var light_color = vec3<f32>(0.3, 0.0, 1.0);
    var light_intensity = 1.0;

    var t = 0.0;

    var ray_start = vec3<f32>(15.0 * rsin, 1.0, 15.0 * -rcos);

    var col = vec4<f32>(0.0, 0.0, 0.0, 1.0);
    var surf = 0;

    var p = ray_start;

    for (var i = 0; i < 1000; i = i + 1) {
        p += dir * 0.05;
        let f = fxz(p.x, p.z);

        if (p.y < f) {
            col.b = 1.0 - (1.0 - col.z) * (1.0 - (f - p.y) * 0.0002);

            let x0 = fxz(p.x - 0.01, p.z);
            let x1 = fxz(p.x + 0.01, p.z);
            let z0 = fxz(p.x, p.z - 0.01);
            let z1 = fxz(p.x, p.z + 0.01);
            let normal = normalize(vec3<f32>(x1 - x0, 0.04, z1 - z0));

            if (surf == 0) {
                let r0water = (1.0 - 1.33) / (1.0 + 1.33);
                let r0water2 = r0water * r0water;
                let cos_theta = dot(normalize(normal + -dir), reflect(-dir, normal));
                let fresnel = max(r0water2 + (1.0 - r0water2) * pow(1.0 - cos_theta, 5.0), 0.0);
                col.r += fresnel * 0.9;
                col.g += fresnel * 0.9;
            }

            surf += 1;
        }
    }

    return col;
}

@fragment
fn fs_main(in: VertexOutput) -> @location(0) vec4<f32> {
    return color(in.tex_coords);
}