struct CameraUniform {
    view_proj: mat4x4<f32>,
};

@group(0) @binding(0)
var<uniform> camera: CameraUniform;

struct SpecialUniform {
    t: f32,
    center_x: f32,
    center_y: f32,
};

struct VertexInput {
    @location(0) position: vec3<f32>,
    @location(1) tex_coords: vec2<f32>,
}

struct VertexOutput {
    @builtin(position) clip_position: vec4<f32>,
    @location(0) tex_coords: vec2<f32>,
}

@vertex
fn vs_main(
    model: VertexInput,
) -> VertexOutput {
    var out: VertexOutput;
    out.tex_coords = model.tex_coords;
    out.clip_position = camera.view_proj * vec4<f32>(model.position, 1.0);
    return out;
}

@group(1) @binding(0)
var<uniform> special: SpecialUniform;

fn color(
    tex_coords: vec2<f32>,
) -> vec4<f32> {
    var x = tex_coords.x;
    var y = tex_coords.y;

    var scale = 5.0 / pow(1.34, special.t);
    var cx = (x - 0.5) * scale + special.center_x;
    var cy = (y - 0.5) * scale + special.center_y;

    var c = vec2<f32>(cx, cy);
    var z = vec2<f32>(cx, cy);

    var i = 0.0;
    var iter = 20.0 + pow(special.t, 2.1);

    loop {
        if (i >= iter) {
            return vec4<f32>(0.0, 0.0, 0.0, 1.0);
        }

        var ix = (z.x * z.x - z.y * z.y) + c.x;
        var iy = (z.y * z.x + z.x * z.y) + c.y;

        if (ix * ix + iy * iy > 4.0) {
            break;
        }

        z.x = ix;
        z.y = iy;

        i += 1.0;
    }

    i /= iter;

    var r = sin(i * 1.0);
    var g = sin(i * 3.0);
    var b = sin(i * 9.0);

    return vec4<f32>(r, g, b, 1.0);
}

@fragment
fn fs_main(in: VertexOutput) -> @location(0) vec4<f32> {
    return color(in.tex_coords);
}