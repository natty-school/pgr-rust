use std::borrow::Cow;
use std::path::Path;
use std::sync::{Arc, Mutex};

use cgmath::{Matrix4, SquareMatrix};
use egui::FontDefinitions;
use egui_wgpu_backend::ScreenDescriptor;
use egui_winit_platform::{Platform, PlatformDescriptor};
use log::{error, warn};
use notify::{RecursiveMode, Watcher};
use wgpu::util::DeviceExt;
use wgpu::{Backends, BlendState, CompositeAlphaMode, RequestAdapterOptions, ShaderModule};
use winit::dpi::PhysicalSize;
use winit::event_loop::ControlFlow;

fn main() {
    env_logger::builder()
        .filter_level(log::LevelFilter::Warn)
        .init();
    pollster::block_on(app_main());
}

async fn app_main() {
    let event_loop = winit::event_loop::EventLoop::new();

    let window = winit::window::WindowBuilder::new()
        .with_title("PGR!")
        .with_inner_size(PhysicalSize::new(800, 600))
        .build(&event_loop)
        .unwrap();

    warn!("Window created.");

    let mut app = App::new(&window).await;

    warn!("App created.");

    event_loop.run(move |event, _, control_flow| {
        app.egui_platform.handle_event(&event);

        match event {
            winit::event::Event::WindowEvent {
                ref event,
                window_id,
            } if window_id == window.id() => match event {
                winit::event::WindowEvent::CloseRequested => *control_flow = ControlFlow::Exit,
                winit::event::WindowEvent::ScaleFactorChanged { new_inner_size, .. } => {
                    app.resize(**new_inner_size);
                }
                winit::event::WindowEvent::Resized(size) => {
                    app.resize(*size);
                }
                _ => (),
            },
            winit::event::Event::RedrawRequested(window_id) if window_id == window.id() => {
                match app.render(&window) {
                    Ok(_) => {}
                    Err(wgpu::SurfaceError::Lost) => app.resize(PhysicalSize::new(
                        app.surface_config.width,
                        app.surface_config.height,
                    )),
                    Err(wgpu::SurfaceError::OutOfMemory) => *control_flow = ControlFlow::Exit,
                    Err(e) => eprintln!("{:?}", e),
                }
            }
            winit::event::Event::MainEventsCleared => {
                window.request_redraw();
            }
            _ => (),
        }
    });
}

#[derive(Debug, PartialEq, Eq, Copy, Clone)]
enum RenderMode {
    Mandelbrot,
    RT,
    RayMarch,
}

impl RenderMode {
    fn get_shader(&self) -> &'static str {
        match self {
            RenderMode::Mandelbrot => "shaders/basic/mandelbrot.wgsl",
            RenderMode::RT => "shaders/basic/rt.wgsl",
            RenderMode::RayMarch => "shaders/basic/raymarch.wgsl",
        }
    }
}

struct App {
    surface: wgpu::Surface,
    adapter: wgpu::Adapter,
    device: Arc<wgpu::Device>,
    queue: wgpu::Queue,
    shader: Arc<Mutex<wgpu::ShaderModule>>,
    surface_config: wgpu::SurfaceConfiguration,
    shader_watcher: notify::RecommendedWatcher,
    egui_rpass: egui_wgpu_backend::RenderPass,
    egui_platform: Platform,
    render_mode: RenderMode,
    time: f32,
    center_x: f32,
    center_y: f32,
    center_x_str: String,
    center_y_str: String,
    last_frame: Option<std::time::Instant>,
}

#[rustfmt::skip]
pub const OPENGL_TO_WGPU_MATRIX: Matrix4<f32> = Matrix4::new(
    1.0, 0.0, 0.0, 0.0,
    0.0, 1.0, 0.0, 0.0,
    0.0, 0.0, 0.5, 0.0,
    0.0, 0.0, 0.5, 1.0,
);

#[repr(C)]
#[derive(Copy, Clone, bytemuck::Pod, bytemuck::Zeroable)]
struct Vertex {
    position: [f32; 3],
    uv: [f32; 2],
}

#[repr(C)]
#[derive(Debug, Copy, Clone, bytemuck::Pod, bytemuck::Zeroable)]
struct TransMatUniform {
    view_proj: [[f32; 4]; 4],
}

impl TransMatUniform {
    fn new() -> Self {
        Self {
            view_proj: Matrix4::identity().into(),
        }
    }

    fn new_from(view_proj: Matrix4<f32>) -> Self {
        Self {
            view_proj: view_proj.into(),
        }
    }
}

#[repr(C)]
#[derive(Debug, Copy, Clone, bytemuck::Pod, bytemuck::Zeroable)]
struct TimeUniform {
    time: f32,
    center_x: f32,
    center_y: f32,
}

impl Vertex {
    const ATTRIBS: [wgpu::VertexAttribute; 2] =
        wgpu::vertex_attr_array![0 => Float32x3, 1 => Float32x2];

    #[inline]
    fn new(position: [f32; 3], uv: [f32; 2]) -> Self {
        Self { position, uv }
    }

    fn desc<'a>() -> wgpu::VertexBufferLayout<'a> {
        wgpu::VertexBufferLayout {
            array_stride: std::mem::size_of::<Vertex>() as wgpu::BufferAddress,
            step_mode: wgpu::VertexStepMode::Vertex,
            attributes: &Self::ATTRIBS,
        }
    }
}

fn switch_shader(shader_path: &Path, device: &wgpu::Device) -> Option<ShaderModule> {
    let shader_code = std::fs::read_to_string(shader_path);

    if let Err(e) = shader_code {
        error!("Failed to read shader file: {}", e);
        return None;
    }

    let code = shader_code.unwrap();

    let module = naga::front::wgsl::parse_str(&code);

    if let Err(e) = module {
        error!("Failed to validate shader file:");
        e.emit_to_stderr(&code);
        return None;
    }

    let module = module.unwrap();

    let valid = naga::valid::Validator::new(
        naga::valid::ValidationFlags::all(),
        naga::valid::Capabilities::all(),
    )
    .validate(&module);

    if let Err(e) = valid {
        error!("Failed to validate shader file:");
        e.emit_to_stderr(&code);
        return None;
    }

    let shader_new = wgpu::ShaderModuleDescriptor {
        label: None,
        source: wgpu::ShaderSource::Naga(Cow::Owned(module)),
    };

    Some(device.create_shader_module(shader_new))
}

fn create_shader_watcher(
    shader_path: &Path,
    device_arc: Arc<wgpu::Device>,
    shader_arc: Arc<Mutex<wgpu::ShaderModule>>,
) -> notify::RecommendedWatcher {
    let path_clone = Arc::new(shader_path.to_path_buf());
    let mut watcher =
        notify::recommended_watcher(move |res: Result<notify::Event, notify::Error>| {
            warn!("Watcher: {:?}", res);

            match res {
                Ok(event) => {
                    if let notify::event::EventKind::Modify(_) = event.kind {
                        warn!("Reloading shader.");

                        if let Some(new_shader) = switch_shader(&path_clone, &device_arc) {
                            *shader_arc.lock().unwrap() = new_shader;
                            warn!("Shader reloaded.");
                        }
                    }
                }
                Err(e) => println!("watch error: {:?}", e),
            }
        })
        .unwrap();

    watcher
        .watch(shader_path, RecursiveMode::NonRecursive)
        .unwrap();

    watcher
}

impl App {
    async fn new(window: &winit::window::Window) -> Self {
        let instance = wgpu::Instance::new(Backends::all());

        let size = window.inner_size();

        let surface = unsafe { instance.create_surface(window) };

        let adapter = instance
            .request_adapter(&RequestAdapterOptions {
                power_preference: wgpu::PowerPreference::HighPerformance,
                compatible_surface: Some(&surface),
                force_fallback_adapter: false,
            })
            .await
            .unwrap();

        let (device, queue) = adapter
            .request_device(
                &wgpu::DeviceDescriptor {
                    label: None,
                    features: wgpu::Features::empty(),
                    limits: wgpu::Limits::default(),
                },
                None,
            )
            .await
            .unwrap();

        let device = Arc::new(device);

        let formats = surface.get_supported_formats(&adapter);

        println!("Supported formats: {:#?}", formats);

        let surface_config = wgpu::SurfaceConfiguration {
            usage: wgpu::TextureUsages::RENDER_ATTACHMENT,
            format: formats[0],
            width: size.width,
            height: size.height,
            present_mode: wgpu::PresentMode::Mailbox,
            alpha_mode: CompositeAlphaMode::Auto,
        };

        surface.configure(&device, &surface_config);

        let render_mode = RenderMode::Mandelbrot;
        let shader_code = std::fs::read_to_string(render_mode.get_shader()).unwrap();

        let shader = Arc::new(Mutex::new(device.create_shader_module(
            wgpu::ShaderModuleDescriptor {
                label: None,
                source: wgpu::ShaderSource::Wgsl(Cow::from(shader_code)),
            },
        )));

        let shader_watcher = create_shader_watcher(
            Path::new(render_mode.get_shader()),
            device.clone(),
            shader.clone(),
        );

        let egui_rpass = egui_wgpu_backend::RenderPass::new(&device, surface_config.format, 1);

        let platform = Platform::new(PlatformDescriptor {
            physical_width: size.width as u32,
            physical_height: size.height as u32,
            scale_factor: window.scale_factor(),
            font_definitions: FontDefinitions::default(),
            style: Default::default(),
        });

        Self {
            surface,
            adapter,
            device,
            shader,
            queue,
            surface_config,
            egui_rpass,
            egui_platform: platform,
            render_mode,
            shader_watcher,
            time: 0.0,
            center_x: -0.348425,
            center_y: -0.60652,
            center_x_str: "-0.348425".to_string(),
            center_y_str: "-0.60652".to_string(),
            last_frame: None,
        }
    }

    fn resize(&mut self, new_size: PhysicalSize<u32>) {
        if new_size.width > 0 && new_size.height > 0 {
            self.surface_config.width = new_size.width;
            self.surface_config.height = new_size.height;

            self.surface.configure(&self.device, &self.surface_config);
        }
    }

    fn render(&mut self, window: &winit::window::Window) -> Result<(), wgpu::SurfaceError> {
        let frame_time = if let Some(last_frame) = self.last_frame {
            let delta = last_frame.elapsed().as_secs_f32();

            self.time += delta;
            delta
        } else {
            0.0
        };

        self.last_frame = Some(std::time::Instant::now());
        self.egui_platform.update_time(self.time as f64);

        let frame = self.surface.get_current_texture()?;
        let frame_view = frame.texture.create_view(&Default::default());

        let bind_group_layout =
            self.device
                .create_bind_group_layout(&wgpu::BindGroupLayoutDescriptor {
                    label: None,
                    entries: &[wgpu::BindGroupLayoutEntry {
                        binding: 0,
                        visibility: wgpu::ShaderStages::VERTEX,
                        ty: wgpu::BindingType::Buffer {
                            ty: wgpu::BufferBindingType::Uniform,
                            has_dynamic_offset: false,
                            min_binding_size: None,
                        },
                        count: None,
                    }],
                });

        let special_bind_group_layout =
            self.device
                .create_bind_group_layout(&wgpu::BindGroupLayoutDescriptor {
                    label: None,
                    entries: &[wgpu::BindGroupLayoutEntry {
                        binding: 0,
                        visibility: wgpu::ShaderStages::FRAGMENT,
                        ty: wgpu::BindingType::Buffer {
                            ty: wgpu::BufferBindingType::Uniform,
                            has_dynamic_offset: false,
                            min_binding_size: None,
                        },
                        count: None,
                    }],
                });

        let pipeline_layout = self
            .device
            .create_pipeline_layout(&wgpu::PipelineLayoutDescriptor {
                label: None,
                bind_group_layouts: &[&bind_group_layout, &special_bind_group_layout],
                push_constant_ranges: &[],
            });

        let mut shader = self.shader.lock().unwrap();

        let pipeline = self
            .device
            .create_render_pipeline(&wgpu::RenderPipelineDescriptor {
                label: None,
                layout: Some(&pipeline_layout),
                vertex: wgpu::VertexState {
                    module: &shader,
                    entry_point: "vs_main",
                    buffers: &[Vertex::desc()],
                },
                fragment: Some(wgpu::FragmentState {
                    module: &shader,
                    entry_point: "fs_main",
                    targets: &[Some(wgpu::ColorTargetState {
                        format: self.surface_config.format,
                        blend: Some(BlendState::ALPHA_BLENDING),
                        write_mask: wgpu::ColorWrites::ALL,
                    })],
                }),
                primitive: wgpu::PrimitiveState {
                    topology: wgpu::PrimitiveTopology::TriangleList,
                    strip_index_format: None,
                    front_face: wgpu::FrontFace::Ccw,
                    cull_mode: None,
                    polygon_mode: wgpu::PolygonMode::Fill,
                    conservative: false,
                    unclipped_depth: false,
                },
                depth_stencil: None,
                multisample: wgpu::MultisampleState {
                    count: 1,
                    mask: !0,
                    alpha_to_coverage_enabled: false,
                },
                multiview: None,
            });

        let quad = &[
            Vertex::new([-0.5, 0.5, 0.0], [0.0, 0.0]),
            Vertex::new([-0.5, -0.5, 0.0], [0.0, 1.0]),
            Vertex::new([0.5, -0.5, 0.0], [1.0, 1.0]),
            Vertex::new([0.5, -0.5, 0.0], [1.0, 1.0]),
            Vertex::new([0.5, 0.5, 0.0], [1.0, 0.0]),
            Vertex::new([-0.5, 0.5, 0.0], [0.0, 0.0]),
        ];

        let buffer = self
            .device
            .create_buffer_init(&wgpu::util::BufferInitDescriptor {
                label: None,
                contents: bytemuck::cast_slice(quad),
                usage: wgpu::BufferUsages::VERTEX,
            });

        let proj: Matrix4<f32> = cgmath::perspective(
            cgmath::Deg(60.0),
            self.surface_config.width as f32 / self.surface_config.height as f32,
            0.01,
            100.0,
        );

        let transformation = Matrix4::from_translation(cgmath::Vector3::new(0.0, 0.0, -0.75));

        let view_proj = OPENGL_TO_WGPU_MATRIX * proj * transformation;

        let view_proj_struct = TransMatUniform::new_from(view_proj);

        let view_proj_buf = self
            .device
            .create_buffer_init(&wgpu::util::BufferInitDescriptor {
                label: None,
                contents: bytemuck::cast_slice(&[view_proj_struct]),
                usage: wgpu::BufferUsages::UNIFORM | wgpu::BufferUsages::COPY_DST,
            });

        let bind_group = self.device.create_bind_group(&wgpu::BindGroupDescriptor {
            label: None,
            layout: &bind_group_layout,
            entries: &[wgpu::BindGroupEntry {
                binding: 0,
                resource: view_proj_buf.as_entire_binding(),
            }],
        });

        let time_struct = TimeUniform {
            time: self.time,
            center_x: self.center_x,
            center_y: self.center_y,
        };

        let time_uniform_buf = self
            .device
            .create_buffer_init(&wgpu::util::BufferInitDescriptor {
                label: None,
                contents: bytemuck::cast_slice(&[time_struct]),
                usage: wgpu::BufferUsages::UNIFORM | wgpu::BufferUsages::COPY_DST,
            });

        let time_uniform_bind_group = self.device.create_bind_group(&wgpu::BindGroupDescriptor {
            label: None,
            layout: &special_bind_group_layout,
            entries: &[wgpu::BindGroupEntry {
                binding: 0,
                resource: time_uniform_buf.as_entire_binding(),
            }],
        });

        let mut encoder = self.device.create_command_encoder(&Default::default());

        {
            let mut render_pass = encoder.begin_render_pass(&wgpu::RenderPassDescriptor {
                label: None,
                color_attachments: &[Some(wgpu::RenderPassColorAttachment {
                    view: &frame_view,
                    resolve_target: None,
                    ops: wgpu::Operations {
                        load: wgpu::LoadOp::Clear(wgpu::Color {
                            r: 0.1,
                            g: 0.2,
                            b: 0.3,
                            a: 1.0,
                        }),
                        store: true,
                    },
                })],
                depth_stencil_attachment: None,
            });

            render_pass.set_pipeline(&pipeline);
            render_pass.set_vertex_buffer(0, buffer.slice(..));
            render_pass.set_bind_group(0, &bind_group, &[]);
            render_pass.set_bind_group(1, &time_uniform_bind_group, &[]);
            render_pass.draw(0..(quad.len() as u32), 0..1);
        }

        self.egui_platform.begin_frame();

        let egui_ctx = self.egui_platform.context();

        egui::Window::new("Debug Info").show(&egui_ctx, |ui| {
            ui.label(format!("Device: {:#?}", self.adapter.get_info()));
            ui.label(format!("Frame time: {:.2}", frame_time * 1000.0));

            ui.separator();

            ui.label("Render Mode");

            let old_mode = self.render_mode;

            ui.radio_value(&mut self.render_mode, RenderMode::Mandelbrot, "Mandelbrot");
            ui.radio_value(&mut self.render_mode, RenderMode::RT, "RayTracing");
            ui.radio_value(&mut self.render_mode, RenderMode::RayMarch, "RayMarching");

            if old_mode != self.render_mode {
                self.time = 0.0;

                if let Some(new_shader) = switch_shader(
                    Path::new(self.render_mode.get_shader()),
                    self.device.as_ref(),
                ) {
                    *shader = new_shader;
                    warn!("Shader switched: {}", self.render_mode.get_shader());
                }

                self.shader_watcher = create_shader_watcher(
                    Path::new(self.render_mode.get_shader()),
                    self.device.clone(),
                    self.shader.clone(),
                );
            }

            ui.separator();

            match self.render_mode {
                RenderMode::Mandelbrot => {
                    ui.label("Mandelbrot Settings");

                    ui.horizontal(|ui| {
                        ui.label("Center X");

                        if ui.button("-").clicked() {
                            self.center_x -= 0.1 / 1.34f32.powf(self.time);
                            self.center_x_str = self.center_x.to_string();
                        }

                        ui.text_edit_singleline(&mut self.center_x_str);

                        if ui.button("+").clicked() {
                            self.center_x += 0.1 / 1.34f32.powf(self.time);
                            self.center_x_str = self.center_x.to_string();
                        }

                        self.center_x = self.center_x_str.parse().unwrap_or(self.center_x);
                    });

                    ui.horizontal(|ui| {
                        ui.label("Center Y");

                        if ui.button("-").clicked() {
                            self.center_y -= 0.1 / 1.34f32.powf(self.time);
                            self.center_y_str = self.center_y.to_string();
                        }

                        ui.text_edit_singleline(&mut self.center_y_str);

                        if ui.button("+").clicked() {
                            self.center_y += 0.1 / 1.34f32.powf(self.time);
                            self.center_y_str = self.center_y.to_string();
                        }

                        self.center_y = self.center_y_str.parse().unwrap_or(self.center_y);
                    });
                }
                RenderMode::RT => {
                    ui.label("RayTracing Settings");
                }
                RenderMode::RayMarch => {
                    ui.label("RayMarching Settings");
                }
            }

            ui.separator();

            ui.label(format!("Time: {}", self.time));

            let bttn = ui.button("Restart time").on_hover_text("Restart time");

            if bttn.clicked() {
                self.time = 0.0;
            }
        });

        let full_output = self.egui_platform.end_frame(Some(window));
        let paint_jobs = self.egui_platform.context().tessellate(full_output.shapes);

        let screen_descriptor = ScreenDescriptor {
            physical_width: self.surface_config.width,
            physical_height: self.surface_config.height,
            scale_factor: window.scale_factor() as f32,
        };

        let tdelta: egui::TexturesDelta = full_output.textures_delta;
        self.egui_rpass
            .add_textures(&self.device, &self.queue, &tdelta)
            .expect("add texture ok");

        self.egui_rpass
            .update_buffers(&self.device, &self.queue, &paint_jobs, &screen_descriptor);

        self.egui_rpass
            .execute(
                &mut encoder,
                &frame_view,
                &paint_jobs,
                &screen_descriptor,
                None,
            )
            .unwrap();

        self.queue.submit(std::iter::once(encoder.finish()));
        frame.present();

        self.egui_rpass
            .remove_textures(tdelta)
            .expect("remove texture ok");

        Ok(())
    }
}
